package com.example.hilterror

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel


open class ObservableAndroidViewModel @ViewModelInject constructor() : ViewModel(), Observable {
	//TAKEN FROM https://developer.android.com/topic/libraries/data-binding/architecture#observable-viewmodel
	private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()

	override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
		callbacks.add(callback)
	}

	override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
		callbacks.remove(callback)
	}

	/**
	 * Notifies observers that all properties of this instance have changed.
	 */
	fun notifyChange() {
		callbacks.notifyCallbacks(this, 0, null)
	}

	/**
	 * Notifies observers that a specific property has changed. The getter for the
	 * property that changes should be marked with the @Bindable annotation to
	 * generate a field in the BR class to be used as the fieldId parameter.
	 *
	 * @param fieldId The generated BR id for the Bindable field.
	 */
	fun notifyPropertyChanged(fieldId: Int) {
		callbacks.notifyCallbacks(this, fieldId, null)
	}


}