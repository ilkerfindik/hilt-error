package com.example.hilterror.helpers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

object FragmentHelper{
	fun hookUpFragmentForView(activity: FragmentActivity, fragmentToHook: Fragment, viewId: Int){
		val fragmentTransaction = activity.supportFragmentManager.beginTransaction()
		fragmentTransaction.add(viewId, fragmentToHook)
		fragmentTransaction.commit()
	}
}