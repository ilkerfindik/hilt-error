package com.example.hilterror.activities.myactivity

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import com.example.hilterror.ObservableAndroidViewModel
import com.example.hilterror.db.local.MyDatabase
import com.example.hilterror.db.local.entity.MyEntity
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

class MyViewModel @ViewModelInject constructor(var db: MyDatabase) : ObservableAndroidViewModel() {

	var myEntities: List<MyEntity?> = emptyList()
	var entitiesFetched = false
	private var entityWatch: Disposable? = null


	init {
		getMyEntities()
	}

	private fun getMyEntities() {
		entityWatch = db.myDao()?.loadAll()?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.subscribe({
					if (it != null) myEntities = it
					entitiesFetched = true
					notifyChange()
				}, { error ->
					Log.e("myErrors", "New error", error)
				})
	}

	override fun onCleared() {
		entityWatch?.dispose()

		super.onCleared()
	}

	fun insertNewEntity() {
		db.myDao()?.insertAll(MyEntity(myEntities.size +1, "New Entity", "Some Location" ))
			?.subscribeOn(Schedulers.io())
			?.observeOn(AndroidSchedulers.mainThread())
			?.subscribe {
				Log.d("myLogs", "Added New Dummy MyEntity")
				notifyChange()
			}


	}
}
