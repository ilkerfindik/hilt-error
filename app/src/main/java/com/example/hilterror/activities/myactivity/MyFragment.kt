package com.example.hilterror.activities.myactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.hilterror.R
import com.example.hilterror.databinding.FragmentMyfragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyFragment : Fragment(){
	private lateinit var binding: FragmentMyfragmentBinding
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myfragment, container, false)
		binding.emptyStateCTAClicked = getEmptyStateCTAOnClickListener()
		binding.viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

		return binding.root
	}

	fun getEmptyStateCTAOnClickListener() = View.OnClickListener { view ->
		binding.viewModel?.insertNewEntity()
		//startActivity(Intent(context, ChooseVideoSourceActivity::class.java))
	}

}