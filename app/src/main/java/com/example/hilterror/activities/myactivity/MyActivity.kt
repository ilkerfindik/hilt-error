package com.example.hilterror.activities.myactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.hilterror.R
import com.example.hilterror.helpers.FragmentHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_myactivity)

		FragmentHelper.hookUpFragmentForView(this, MyFragment(), R.id.my_entities_fragment_area)

	}


}