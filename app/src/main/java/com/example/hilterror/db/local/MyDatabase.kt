package com.example.hilterror.db.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hilterror.db.local.dao.MyDao
import com.example.hilterror.db.local.entity.MyEntity


@Database(entities = arrayOf(MyEntity::class), version = 1)
abstract class MyDatabase : RoomDatabase() {
	abstract fun myDao(): MyDao?

	companion object {
		val dbName = "my_db"
	}
}