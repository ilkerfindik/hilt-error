package com.example.hilterror.db.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = MyEntity.tableName)
data class MyEntity(@PrimaryKey(autoGenerate = true) val id: Int, val name:String, val videoLocation:String){
	companion object {
		const val tableName = "myEntities"
	}
}