package com.example.hilterror.db.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.hilterror.db.local.entity.MyEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable

private const val tableName = MyEntity.tableName

@Dao interface MyDao {
	@Query("SELECT * FROM $tableName")
	fun loadAll(): Observable<List<MyEntity?>>?

	@Insert
	fun insertAll(vararg myEntities: MyEntity?): Completable

	@Delete
	fun delete(myEntity: MyEntity?)
}