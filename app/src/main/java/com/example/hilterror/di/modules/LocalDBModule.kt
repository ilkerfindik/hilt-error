package com.example.hilterror.di.modules

import android.content.Context
import androidx.room.Room
import com.example.hilterror.db.local.MyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object LocalDBModule {

	@Provides
	@Singleton
	fun provideLocalDB(@ApplicationContext appContext: Context) = Room
			.databaseBuilder(appContext, MyDatabase::class.java, MyDatabase.dbName)
			.build()

}